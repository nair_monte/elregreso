using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{
    public GameObject jugador;
    public Vector3 offset;
    void Start()
    {
        offset = transform.position - jugador.transform.position;
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        transform.position = jugador.transform.position + offset;
    }
}
