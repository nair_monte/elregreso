using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    private Rigidbody rb;

    public float rapidez;


    public TMPro.TMP_Text txtContOro;
    public TMPro.TMP_Text txtWin;
    private int contOros;



    private bool enPiso;
    private int contSaltos;
    public float magnitudSalto;

    public float rotacionVelocidad;
    
    private Vector3 vectorMovimiento;

    public Camera mainCamera;
    
    private Vector3 forward;
    private Vector3 right;

    private Vector3 movePlayer;
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        txtContOro.text = "0";
        txtWin.gameObject.SetActive(false);

        contSaltos = 0;
    }

    private void Update()
    {
        
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");

        if (movimientoHorizontal != 0 || movimientoVertical != 0)
        {
                     //vectorMovimiento = new Vector3(movimientoVertical, 0, movimientoHorizontal);


                          forward = mainCamera.transform.forward;
                     forward.y = 0;
                     forward.Normalize();

                         right = mainCamera.transform.right;
                     right.y = 0;
                     right.Normalize();

            
           

                        //vectorMovimiento =  forward*movimientoVertical    +   right*movimientoHorizontal;
                        vectorMovimiento = new Vector3(movimientoHorizontal, 0, movimientoVertical);

                     //0 y 1, uniforme para que no nos movamos mas rapido en diagonal
                        vectorMovimiento.Normalize();


            
                    movePlayer = vectorMovimiento.x * right + vectorMovimiento.z * forward;

            //mire a donde se va a mover
            transform.LookAt(transform.position + movePlayer);

            
                     transform.position = transform.position + movePlayer * rapidez * Time.deltaTime;
        }

        



        //si se esta moviendo la rotacion va a interpolar entre el punto actual y el punto de la direccion del teclado
        //lookrotation devuelve la rotacion necesaria para mirar a un punto en concreto 
        if (vectorMovimiento != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(vectorMovimiento), rotacionVelocidad * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
         
        }




        txtContOro.text = contOros.ToString();
        if (contOros == 61)
        {
            txtWin.gameObject.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.Space) &&  (enPiso || (contSaltos < 1) )  )
        {
            rb.AddForce (Vector3.up * magnitudSalto, ForceMode.Impulse);


            enPiso = false;
            contSaltos++;
        }
    }

    private void FixedUpdate()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            other.gameObject.SetActive(false);
            contOros++;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "piso")
        {
            enPiso = true;
            contSaltos = 0;
        }
    }
}
