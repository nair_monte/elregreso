using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ControlCheckpoints : MonoBehaviour
{

   public GameObject Checkpoint;
    public int contCheckpoint;

    // Start is called before the first frame update
    void Start()
    {
        contCheckpoint = 0;
    }

    // Update is called once per frame
    void Update()
    {

        
        
        
        if (contCheckpoint == 0)
        {
            Checkpoint.transform.position = new Vector3(5,2,-3);
        }

        if (contCheckpoint == 1)
        {
            Checkpoint.transform.position = new Vector3(-50, 2, -3);
        }

        if (contCheckpoint == 2)
        {
            Checkpoint.transform.position = new Vector3(-27,26, 50);
        }

        if (contCheckpoint == 3)
        {
            Checkpoint.transform.position = new Vector3(133.5f, 25.71f, 49.92f);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "respawnPiso")
        {
            transform.position = Checkpoint.transform.position;
        }

        if (other.gameObject.name == "cp1")
        {
            contCheckpoint++;
          
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.name == "cp2")
        {
            contCheckpoint++;

            other.gameObject.SetActive(false);
        }

        if (other.gameObject.name == "cp3")
        {
            contCheckpoint++;

            other.gameObject.SetActive(false);
        }

    }
}
